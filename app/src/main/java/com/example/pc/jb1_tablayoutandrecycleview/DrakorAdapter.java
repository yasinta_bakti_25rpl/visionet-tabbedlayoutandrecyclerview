package com.example.pc.jb1_tablayoutandrecycleview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pc.jb1_tablayoutandrecycleview.model.CategoriItem;
import com.squareup.picasso.Picasso;

public class DrakorAdapter extends RecyclerView.Adapter<DrakorAdapter.ViewHolder> {

    CategoriItem[] categoriItems;
    Context context;

    public DrakorAdapter(CategoriItem[] categoriItems, Context context) {
        this.categoriItems = categoriItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drakor_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvTitle.setText(categoriItems[position].getStrCategory());
        Picasso.get().load(categoriItems[position].getStrCategoryThumb()).into(holder.ivFoto);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {
                Toast.makeText(context, "You click " + categoriItems[position].getStrCategory(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (categoriItems != null)
            return categoriItems.length;
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivFoto;
        TextView tvTitle;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvTitle = itemView.findViewById(R.id.textViewJudul);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}

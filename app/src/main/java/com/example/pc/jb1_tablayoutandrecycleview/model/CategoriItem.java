package com.example.pc.jb1_tablayoutandrecycleview.model;

import com.google.gson.annotations.SerializedName;

public class CategoriItem {
    @SerializedName("strCategory")
    public String strCategory;

    @SerializedName("strCategoryThumb")
    public String strCategoryThumb;

    public String getStrCategory() {
        return strCategory;
    }

    public String getStrCategoryThumb() {
        return strCategoryThumb;
    }
}

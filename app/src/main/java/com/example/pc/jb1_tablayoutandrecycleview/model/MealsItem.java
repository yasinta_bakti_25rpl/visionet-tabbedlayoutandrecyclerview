package com.example.pc.jb1_tablayoutandrecycleview.model;

import com.google.gson.annotations.SerializedName;

public class MealsItem {
    @SerializedName("strMeal")
    public String strMeal;

    @SerializedName("strMealThumb")
    public String strMealThumb;

    @SerializedName("strCategory")
    public String strCategory;

    @SerializedName("strArea")
    public String strArea;

    @SerializedName("strInstructions")
    public String strInstructions;

    public String getStrMeal() {
        return strMeal;
    }

    public String getStrMealThumb() {
        return strMealThumb;
    }

    public String getStrCategory() {
        return strCategory;
    }

    public String getStrArea() {
        return strArea;
    }

    public String getStrInstructions() {
        return strInstructions;
    }
}

package com.example.pc.jb1_tablayoutandrecycleview.model;

import com.google.gson.annotations.SerializedName;

public class CategoriesItem {
    @SerializedName("categories")
    CategoriItem[] categoriItems;

    public CategoriItem[] getCategoriItems() {
        return categoriItems;
    }

}

package com.example.pc.jb1_tablayoutandrecycleview;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pc.jb1_tablayoutandrecycleview.model.MealsItem;
import com.squareup.picasso.Picasso;

public class DrakorVerAdapter extends RecyclerView.Adapter<DrakorVerAdapter.ViewHolder> {
    MealsItem[] mealsItems;
    Context context;

    public DrakorVerAdapter(MealsItem[] mealsItems, Context context) {
        this.mealsItems = mealsItems;
        this.context = context;
    }

    @Override
    public DrakorVerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View h = LayoutInflater.from(parent.getContext()).inflate(R.layout.drakor_list_ver, parent, false);
        DrakorVerAdapter.ViewHolder hv = new DrakorVerAdapter.ViewHolder(h);
        return hv;
    }

    @Override
    public void onBindViewHolder(DrakorVerAdapter.ViewHolder holder, final int position) {
        holder.jdl.setText(mealsItems[position].getStrMeal());
        holder.category.setText(mealsItems[position].getStrCategory());
        holder.area.setText(mealsItems[position].getStrArea());
        Picasso.get().load(mealsItems[position].getStrMealThumb()).into(holder.gbr);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You clicked " + mealsItems[position].getStrMeal(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mealsItems != null)
            return mealsItems.length;
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView gbr;
        TextView jdl;
        CardView cardView;
        TextView category;
        TextView area;

        public ViewHolder(View itemView) {
            super(itemView);
            gbr = itemView.findViewById(R.id.gbr);
            jdl = itemView.findViewById(R.id.meal);
            cardView = itemView.findViewById(R.id.cardview);
            category = itemView.findViewById(R.id.category);
            area = itemView.findViewById(R.id.area);
        }
    }
}


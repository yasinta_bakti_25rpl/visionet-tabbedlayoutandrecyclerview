package com.example.pc.jb1_tablayoutandrecycleview.model;

import com.google.gson.annotations.SerializedName;

public class LatestItem {
    @SerializedName("meals")
    MealsItem[] mealsItems;

    public MealsItem[] getMealsItems() {
        return mealsItems;
    }
}

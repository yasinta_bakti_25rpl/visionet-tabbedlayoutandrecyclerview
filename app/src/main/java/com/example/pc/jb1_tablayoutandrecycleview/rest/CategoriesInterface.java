package com.example.pc.jb1_tablayoutandrecycleview.rest;

import com.example.pc.jb1_tablayoutandrecycleview.model.CategoriesItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriesInterface {
    @GET("categories.php")
    Call<CategoriesItem> getData();
}

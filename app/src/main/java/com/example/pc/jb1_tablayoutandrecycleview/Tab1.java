package com.example.pc.jb1_tablayoutandrecycleview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.jb1_tablayoutandrecycleview.model.CategoriItem;
import com.example.pc.jb1_tablayoutandrecycleview.model.CategoriesItem;
import com.example.pc.jb1_tablayoutandrecycleview.model.LatestItem;
import com.example.pc.jb1_tablayoutandrecycleview.model.MealsItem;
import com.example.pc.jb1_tablayoutandrecycleview.rest.ApiClient;
import com.example.pc.jb1_tablayoutandrecycleview.rest.CategoriesInterface;
import com.example.pc.jb1_tablayoutandrecycleview.rest.LatestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Tab1 extends Fragment {
    ArrayList<Drakor> mList = new ArrayList<>();
    DrakorAdapter mAdapter;
    DrakorVerAdapter vAdapter;
    RecyclerView recyclerView2, recyclerView3;

    CategoriesInterface categoriesInterface;
    Call<CategoriesItem> categoriesItemCall;
    CategoriItem[] categoriItems;

    LatestInterface latestInterface;
    Call<LatestItem> latestItemCall;
    MealsItem[] mealsItem;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.tab1, container, false);

        recyclerView2 = view1.findViewById(R.id.recyclerView2);
        recyclerView3 = view1.findViewById(R.id.recyclerView3);

//        recyclerView = view1.findViewById(R.id.recyclerView2);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        recyclerView.setLayoutManager(layoutManager);
//        mAdapter = new DrakorAdapter(mList, getContext());
//        recyclerView.setAdapter(mAdapter);
        fillData();

        return view1;
    }

    private void fillData() {

        categoriesInterface = ApiClient.getClient().create(CategoriesInterface.class);
        categoriesItemCall = categoriesInterface.getData();

        latestInterface = ApiClient.getClient().create(LatestInterface.class);
        latestItemCall = latestInterface.getData();

        categoriesItemCall.enqueue(new Callback<CategoriesItem>() {
            @Override
            public void onResponse(Call<CategoriesItem> call, Response<CategoriesItem> response) {
                categoriItems = response.body().getCategoriItems();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView2.setLayoutManager(layoutManager);
                mAdapter = new DrakorAdapter(categoriItems, getContext());
                recyclerView2.setAdapter(mAdapter);

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<CategoriesItem> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });

        latestItemCall.enqueue(new Callback<LatestItem>() {
            @Override
            public void onResponse(Call<LatestItem> call, Response<LatestItem> response) {
                mealsItem = response.body().getMealsItems();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerView3.setLayoutManager(layoutManager);
                vAdapter = new DrakorVerAdapter(mealsItem, getContext());
                recyclerView3.setAdapter(vAdapter);

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<LatestItem> call, Throwable t) {
                Log.wtf("hasil", t.getMessage());
            }
        });
//        Resources resources = getResources();
//        String[] arJudul = resources.getStringArray(R.array.places);
//        TypedArray a = resources.obtainTypedArray(R.array.places_picture);
//        Drawable[] arFoto = new Drawable[a.length()];
//        for (int i = 0; i < arFoto.length; i++) {
//            arFoto[i] = a.getDrawable(i);
//        }
//        a.recycle();
//        for (int i = 0; i < arJudul.length; i++) {
//            mList.add(new Drakor(arJudul[i], arFoto[i]));
//        }
//        mAdapter.notifyDataSetChanged();
    }
}

package com.example.pc.jb1_tablayoutandrecycleview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserProfile extends AppCompatActivity {
    EditText nama, email, address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);

        nama = findViewById(R.id.nama);
        Button save_data = findViewById(R.id.save_data);
        Button edit_data = findViewById(R.id.btn_edit);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);

        checkFilled();

        save_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!nama.getText().toString().equals("")
                        && !email.getText().toString().equals("")
                        && !address.getText().toString().equals("")) {
                    Preferences.setNama(getApplicationContext(), nama.getText().toString());
                    Preferences.setEmail(getApplicationContext(), email.getText().toString());
                    Preferences.setAddress(getApplicationContext(), address.getText().toString());
                    Preferences.setFilled(getApplicationContext(), true);
                    nama.setEnabled(false);
                    email.setEnabled(false);
                    address.setEnabled(false);
                }
            }
        });
        edit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nama.setEnabled(true);
                email.setEnabled(true);
                address.setEnabled(true);
            }
        });
    }

    void checkFilled() {
        if (Preferences.getFilled(getApplicationContext())) {
            nama.setText(Preferences.getNama(getApplicationContext()));
            nama.setEnabled(false);
            email.setText(Preferences.getEmail(getApplicationContext()));
            email.setEnabled(false);
            address.setText(Preferences.getAddress(getApplicationContext()));
            address.setEnabled(false);
        }
    }
}

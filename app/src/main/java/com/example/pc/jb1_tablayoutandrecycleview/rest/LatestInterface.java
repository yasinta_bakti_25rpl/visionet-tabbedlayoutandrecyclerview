package com.example.pc.jb1_tablayoutandrecycleview.rest;

import com.example.pc.jb1_tablayoutandrecycleview.model.LatestItem;

import retrofit2.Call;
import retrofit2.http.GET;

public interface LatestInterface {
    @GET("latest.php")
    Call<LatestItem> getData();
}

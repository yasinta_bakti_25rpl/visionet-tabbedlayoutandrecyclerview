package com.example.pc.jb1_tablayoutandrecycleview;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    static final String NAMA = "Nama";
    static final String EMAIL = "Email";
    static final String ADDRESS = "Address";
    static final String FILLED = "Filled";

    /**
     * Pendlakarasian Shared Preferences yang berdasarkan paramater context
     */
    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setNama(Context context, String nama) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(NAMA, nama);
        editor.apply();
    }

    public static String getNama(Context context) {
        return getSharedPreference(context).getString(NAMA, "");
    }

    public static String getEmail(Context context) {
        return getSharedPreference(context).getString(EMAIL, "");
    }

    public static String getAddress(Context context) {
        return getSharedPreference(context).getString(ADDRESS, "");
    }

    public static Boolean getFilled(Context context) {
        return getSharedPreference(context).getBoolean(FILLED, false);
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public static void setAddress(Context context, String address) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(ADDRESS, address);
        editor.apply();
    }

    public static void setFilled(Context context, Boolean filled) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(FILLED, filled);
        editor.apply();
    }
}

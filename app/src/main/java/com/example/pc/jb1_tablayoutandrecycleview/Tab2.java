package com.example.pc.jb1_tablayoutandrecycleview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.jb1_tablayoutandrecycleview.model.LatestItem;
import com.example.pc.jb1_tablayoutandrecycleview.model.MealsItem;
import com.example.pc.jb1_tablayoutandrecycleview.rest.ApiClient;
import com.example.pc.jb1_tablayoutandrecycleview.rest.LatestInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Tab2 extends Fragment {
    ArrayList<Drakor> mList = new ArrayList<>();
    DrakorVerAdapter vAdapter;
    RecyclerView recyclerView;

    LatestInterface latestInterface;
    Call<LatestItem> latestItemCall;
    MealsItem[] mealsItem;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab2, container, false);

//        recyclerView = view.findViewById(R.id.recyclerView);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(layoutManager);
//        vAdapter = new DrakorVerAdapter(mList, getContext());
//        recyclerView.setAdapter(vAdapter);
//        fillData();
        recyclerView = view.findViewById(R.id.recyclerView);
        fillData();
        return view;
    }

    private void fillData() {
        latestInterface = ApiClient.getClient().create(LatestInterface.class);
        latestItemCall = latestInterface.getData();

        latestItemCall.enqueue(new Callback<LatestItem>() {
            @Override
            public void onResponse(Call<LatestItem> call, Response<LatestItem> response) {
                mealsItem = response.body().getMealsItems();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(layoutManager);
                vAdapter = new DrakorVerAdapter(mealsItem, getContext());
                recyclerView.setAdapter(vAdapter);

                vAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<LatestItem> call, Throwable t) {

            }
        });
    }

//    private void fillData() {
//        Resources resources = getResources();
//        String[] arJudul = resources.getStringArray(R.array.places);
//        TypedArray a = resources.obtainTypedArray(R.array.places_picture);
//        Drawable[] arFoto = new Drawable[a.length()];
//        for (int i = 0; i < arFoto.length; i++) {
//            arFoto[i] = a.getDrawable(i);
//        }
//        a.recycle();
//        for (int i = 0; i < arJudul.length; i++) {
//            mList.add(new Drakor(arJudul[i], arFoto[i]));
//        }
//        vAdapter.notifyDataSetChanged();
//    }
}
